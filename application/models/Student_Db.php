<?php

/**
 * Handles 'Students' table calls
 * 
 *
 * @author Steven Mwesigwa @steven7mwesigwa
 */
class Student_Db extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database(); // load and instantiate the database class
    }

    /**
     * Returns an array containing a resultset of all Students.
     * @return array -all student data.
     */
    function get_all_students(): array {
        return $this->db->get('Students')->result();
    }

 /**
  * Adds student data to database
  * @param array $student_data
  * @return bool
  */
    function add_student(array $student_data): bool {
        $this->db->insert('Students', $student_data);
        return TRUE;
    }

    /**
     *  Returns an object containing a resultset of a single Student
     * @param string $student_id
     */
    function get_student(string $student_id) {
        return $this->db->
                        get_where('Students',
                                array('student_id' => $student_id))->row();
    }

    /**
     *  Update Student information
     * @param array $student_data
     * @param string $student_id
     * @return bool
     */
    function update_student(array $student_data, string $student_id): bool {
        $this->db->where('student_id', $student_id);
        $this->db->update('Students', $student_data);
        return TRUE;
    }

    /**
     *  Deletes a Student
     * @param string $student_id
     * @return bool
     */
    function delete_student(string $student_id): bool {
        $this->db->where('student_id', $student_id);
        $this->db->delete('Students');
        return TRUE;
    }

}
