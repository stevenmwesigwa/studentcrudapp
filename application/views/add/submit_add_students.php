<br>
<br>
<br>
<br>
<div class="container">
    <div class="row">
        <div class="col-md-6 d-flex justify-content-center ">
            <form method="post" action="/public/index.php/student/add_student">

                <div class="form-group">
                    <label for="first-name">First name:</label>
                    <input type="text" class="form-control" id="first-name" name="first_name" required>
                </div>
                <div class="form-group">
                    <label for="last-name">Last name:</label>
                    <input type="text" class="form-control" id="last-name" name="last_name" required>
                </div>
                <div class="form-group">
                    <label for="email-address">Email address:</label>
                    <input type="email" class="form-control" id="email-address" aria-describedby="emailHelp" name="email_address" required>
                    <small id="emailHelp" class="form-text text-muted">
                        We'll never share your email with anyone else.
                    </small>
                </div>
                <br>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="confirm-not-robot">
                    <label class="form-check-label" for="confirm-not-robot">Check me out</label>
                </div>
                <br>

                <button type="submit" class="btn btn-primary" >Submit</button>
            </form>
        </div>
    </div>
</div>





