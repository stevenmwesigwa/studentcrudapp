<br>

<h2>Yoo University</h2>

<br>

<ul>
    <table id="table_id" class="display nowrap" style="width:100%">
        <thead>
            <tr>
                <th>Student_id</th>
                <th>First_name</th>
                <th>Last_name</th>
                <th>Email_address</th>
                <th>Edit</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($all_students as $student): ?>
                <tr>
                    <td><?php echo $student->student_id; ?></td>
                    <td><?php echo $student->first_name; ?></td>
                    <td><?php echo $student->last_name; ?></td>
                    <td><?php echo $student->email_address; ?></td>
                    <td>
                        <a href="/public/index.php/student/edit_student_page/<?php echo $student->student_id; ?>"><i class="fa fa-edit fa-2x"  aria-hidden="true" ></i></a>
                        &nbsp;&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="/public/index.php/student/delete_student/<?php echo $student->student_id; ?>"><i class="fa fa-trash fa-2x" aria-hidden="true"></i></a>
                    </td>
                </tr>
            <?php endforeach; ?>

        </tbody>
    </table>
</ul>

<br>

<!--add student-->
<div class="row">
    <div class="col text-center">
        <a class="btn btn-primary btn-lg" href="/public/index.php/student/add_student_page" role="button">Add Student</a>
    </div>
</div>

