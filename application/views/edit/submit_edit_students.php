<br>
<br>
<br>
<br>
<div class="container">
    <div class="row">
        <div class="col-md-6 d-flex justify-content-center ">
            <form method="post" action="/public/index.php/student/update_student">
                <div class="form-group" hidden>
                    <label for="student_id">Student id</label>
                    <input type="text" class="form-control" id="student_id" name="student_id" value="<?php echo $student->student_id; ?>" required>
                </div>

                <div class="form-group">
                    <label for="first_name">First name:</label>
                    <input type="text" class="form-control" id="first_name" name="first_name" value="<?php echo $student->first_name; ?>" required>
                </div>

                <div class="form-group">
                    <label for="last_name">Last name:</label>
                    <input type="text" class="form-control" id="last_name" name="last_name" value="<?php echo $student->last_name; ?>" required>
                </div>
                <div class="form-group">
                    <label for="email_address">Email address:</label>
                    <input type="email" class="form-control" id="email_address" aria-describedby="emailHelp" name="email_address" value="<?php echo $student->email_address; ?>" required>
                    <small id="emailHelp" class="form-text text-muted">
                        We'll never share your email with anyone else.
                    </small>
                </div>
                <br>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="confirm-not-robot">
                    <label class="form-check-label" for="confirm-not-robot">Check me out</label>
                </div>
                <br>

                <button type="submit" class="btn btn-primary" >Submit</button>
            </form>
        </div>
    </div>
</div>





