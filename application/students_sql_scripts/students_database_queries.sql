/**
 * Author:  Steven Mwesigwa @steven7mwesigwa
 * Created: Feb 14, 2019
 */

/**
* Tested for MySQL 5.6 and later
*
*/

-- create a user called 'webstudentdb' with a password 'webstudentdb' and 
-- grant him / her 'ALL PRIVILEGES' necessary for our app.
-- CREATE USER IF NOT EXISTS 'webstudentdb'@'localhost' IDENTIFIED BY 'webstudentdb';
CREATE USER 'webstudentdb'@'localhost' IDENTIFIED BY 'webstudentdb';

GRANT ALL PRIVILEGES ON * . * TO 'webstudentdb'@'localhost';

-- create 'students_registration' database
-- CREATE DATABASE IF NOT EXISTS students_registration CHARACTER SET utf8 COLLATE utf8_general_ci;
CREATE DATABASE students_registration CHARACTER SET utf8 COLLATE utf8_general_ci;

-- set our new 'students_registration' database as current
USE `students_registration`;

-- Drop the 'Students' table if it exists.
DROP TABLE IF EXISTS `Students`;

-- create 'Students' table
CREATE TABLE Students (
    student_id int NOT NULL AUTO_INCREMENT,
    first_name varchar(255) NOT NULL,
    last_name varchar(255) NOT NULL,
    email_address varchar(255) NOT NULL,
    PRIMARY KEY (student_id)
) ENGINE=InnoDB; 

-- Lock database 'Students' table and prepare it for writing
LOCK TABLES `Students` WRITE;

-- insert some Students to work as sample data
INSERT INTO Students (first_name, last_name, email_address)
VALUES ('John', 'Doe', 'john-doe@gmail.com');
INSERT INTO Students (first_name, last_name, email_address)
VALUES ('Eric', 'Emoding', 'eric766@yahoo.com');
INSERT INTO Students (first_name, last_name, email_address)
VALUES ('Okal', 'Tony', 'tonny-78@gmtconsults.com');
INSERT INTO Students (first_name, last_name, email_address)
VALUES ('Tracy', 'Namutaba', 'tracy6namutaba@gmail.com');
INSERT INTO Students (first_name, last_name, email_address)
VALUES ('Kevin', 'Opul', 'quison@yahoo.com');
INSERT INTO Students (first_name, last_name, email_address)
VALUES ('Dorothy', 'Micheal', 'dorothy-m@gmail.com');
INSERT INTO Students (first_name, last_name, email_address)
VALUES ('Kim', 'Song', 'kim-song@mtn-ug.com');

-- Unlock our table
UNLOCK TABLES;

-- 15 FEB 2019 - Steven Mwesigwa 
ALTER TABLE students_registration.students
ADD UNIQUE (email_address);


