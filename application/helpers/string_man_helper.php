<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('test_method')) {

    function test_method($var = '') {
        return $var;
    }

}

/**
 * Returns Sentence case representation of string
 * @param string $string String to convert
 * @return string Converted string
 */
function sentence_case(string $string): string {
    return ucfirst(strtolower(trim($string)));
}
