<?php

/**
 * Creates Student CRUD operations
 * view_students() - Lists all Students.
 * add_student() - Adds a Student to the List.
 * update_student() - Updates  Student's information.
 * delete_student() - Deletes a Student.
 *
 * @author Steven Mwesigwa @steven7mwesigwa
 */
class Student extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Student_Db');
        $this->load->helper('string_man_helper');
    }

    /**
     * Lists all Students.
     * @return void
     */
    function view_students(): void {
        $data['all_students'] = $this->Student_Db->get_all_students();
        $data['title'] = 'Yoo University Student List';

        $this->load->view('templates/view_students/view_students_header', $data);
        $this->load->view('view_students', $data);
        $this->load->view('templates/view_students/view_students_footer', $data);
    }

    /**
     * Adds a Student.
     * @return void
     */
    function add_student(): void {

        $user_data = array(
            'first_name' => sentence_case($this->input->post('first_name')),
            'last_name' => sentence_case($this->input->post('last_name')),
            'email_address' => sentence_case($this->input->post('email_address')),
        );

        if ($this->Student_Db->add_student($user_data)) {
            $this->view_students();
            $this->redirect_to_home_page();
        }
    }

    /**
     * Updates Student Information.
     * @return void
     */
    function update_student(): void {
        $student_data = array(
            'first_name' => $this->input->post('first_name'),
            'last_name' => $this->input->post('last_name'),
            'email_address' => $this->input->post('email_address'),
        );

        $student_id = $this->input->post('student_id');
        if ($this->Student_Db->update_student($student_data, $student_id)) {
            $this->view_students();
            $this->redirect_to_home_page();
        }
    }

    /**
     *  Deletes a Student.
     * @param string $student_id
     * @return void
     */
    function delete_student(string $student_id): void {

        if ($this->Student_Db->delete_student($student_id)) {
            $this->view_students();
            $this->redirect_to_home_page();
        }
    }

    /**
     * Holds form for submitting add students actions
     * @return void
     */
    function add_student_page(): void {
        $this->load->view('templates/header');
        $this->load->view('add/submit_add_students');
        $this->load->view('templates/footer');
    }

    /**
     * Holds form for submitting edit students actions
     * @param string $student_id
     * @return void
     */
    function edit_student_page(string $student_id): void {
        $data['student'] = $this->Student_Db->get_student($student_id);
        if ($data['student'] !== NULL) {
            $this->load->view('templates/header');
            $this->load->view('edit/submit_edit_students', $data);
            $this->load->view('templates/footer');
        }
    }

    /**
     * Redirects to home page
     * @return void
     */
    function redirect_to_home_page(): void {
        $trimmedBaseUrl = rtrim(base_url(), '/');
        $serverPort = $_SERVER['SERVER_PORT'];
        $newBaseUrl = "$trimmedBaseUrl:$serverPort/";
        redirect($newBaseUrl . 'public/index.php/student/view_students');
    }

}
